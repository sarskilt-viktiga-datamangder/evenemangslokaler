## Exempel i CSV, kommaseparerad

<div class="example csvtext">
id,source,type,latitude,longitude,name,description,amenities,accessibilityfeature,event,capacity,opens,closes,openinghours,street,postalcode,city,email,URL,image
<br>
2454,9876543210,PerformingArtsTheater,55.604981,13.003822,Malmö City Theatre,En historisk teater med moderna produktioner,Wi-Fi,Handikapptoalett,https://example.com/event/456, 500,17:00,22:00,"Öppet mån-lör 17:00-22:00, söndagar stängt",Stortorget 1,211 22,Malmö,info@malmotheatre.com,https://www.malmotheatre.com/ ,https://example.com/images/malmotheatre.jpg
</div>

