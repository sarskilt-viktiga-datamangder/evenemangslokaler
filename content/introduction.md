# Introduktion 

Denna specifikation anger vilka fält som är obligatoriska och rekommenderade. T.ex. gäller att man måste ange id, longitude och latitude. 19 attribut kan anges varav 5 är obligatoriska. 

I appendix A finns ett exempel på hur en evenemangslokal uttrycks i CSV. I appendix B uttrycks samma exempel i JSON. 

Denna specifikation definierar en enkel tabulär informationsmodell för evenmangslokaler. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten CSV och JSON. Som grund förutsätts CSV kunna levereras då det är ett praktiskt format och skapar förutsägbarhet för mottagare av informationen. 
