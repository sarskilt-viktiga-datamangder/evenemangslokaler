# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en evenemangslokal och varje kolumn motsvarar en egenskap för den beskrivna evenemangslokalen. 20 attribut är definierade, där de första 5 är obligatoriska. Speciellt viktigt är att man anger de obligatoriska fält i datamodellen som anges. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.

</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**id**](#id)|1|text|**Obligatoriskt** - Identifierare för evenemangslokalen.|
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Organisationsnumret utan mellanslag eller bindesstreck för organisationen.|
|[**type**](#type)|1|[type](#type)|**Obligatoriskt** - Evenemangslokalens typ enligt givna kategorier.|
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Latitude anges per format enligt WGS84.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Longitude anges per format enligt WGS84.|
|[name](#name)|0..1|text|Namnet på evenemangslokalen.|
|[description](#description)|0..1|text|Ytterligare beskrivning av evenemangslokalen.|
|[amenities](#amenities)|0..*|[amenities](#amenities)|Bekvämligheter som tillhör evenemangslokalen.|
|[accessibilityfeature](#accessibilityfeature)|0..*|[accessibilityfeature](#accessibilityfeature)|Tillgänglighetsstöd som finns vid evenemangslokalen.|
|[event](#event)|0..*|URL|Länk till kommande evenemang eller evenemangsschema.|
|[capacity](#capacity)|0..1|heltal|Max antal individer som kan vara i evenemangslokalen.|
|[opens](#opens)|0..1|time|Tid när lokalen öppnar.|
|[closes](#closes)|0..1|time|Tid när lokalen stänger.|
|[openinghours](#openinghours)|0..1|text|Ytterligare beskrivning av evenemangslokalens öppettider.|
|[street](#street)|0..1|text|Gatuadress för evenemangslokalen.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Postnummer för evenemangslokalen.|
|[city](#city)|0..1|text|Postort för evenemangslokalen.|
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om evenemangslokalen.|
|[image](#image)|0..1|[URL](#url)|Länk till bild eller bilder på evenemangslokalen.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där evenemangslokalen presenteras hos den lokala myndigheten eller organisationen.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### time 

En tidpunkt som återkommer på flera dagar i formen hh:mm:ss[Z|(+|-)hh:mm] (se XML-schema för detaljer)[http://www.w3.org/TR/xmlschema-2/#time].

hh:mm:ss eller hh:mm 

Exempel - en sevärdhet som öppnar kl 17:00. 

<div>

17:00

</div>

## Förtydligande av attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Anger identfierare för evenemangslokalen. 

### source

Reguljärt uttryck: **`/^-?\d+$/`**

Anger organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### type    

Anger typen av evenemangslokal enligt givna värden nedan:


[MusicVenue](https://schema.org/MusicVenue)

<div>

[EventVenue](https://schema.org/EventVenue)

<div>

[StadiumOrArena](https://schema.org/StadiumOrArena)

<div>

[PerformingArtsTheater](https://schema.org/PerformingArtsTheater)

### latitude

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`**

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`**

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.

### name

Reguljärt uttryck: **`/.+/`**

Anger namnet på lokalen. 

### description

Reguljärt uttryck: **`/.+/`**

Anger en beskrivning av lokalen. 

### amenities

Anger vilka faciliteter som tillhör lokalen i fritext, exempelvis Wi-Fi

*Vid specifikationens publicering fanns det inte strikta vokabulär att hämta värden från till detta attribut men kommer att inkorpereras direkt i denna specifikation när dessa finns på nationell eller internationell nivå.* 

### accessibilityfeature

Anger vilka tillgänglighetsstöd som tillhör lokalen i fritext, exempelvis ramp.

*Vid specifikationens publicering fanns det inte strikta vokabulär att hämta värden från till detta attribut men kommer att inkorpereras direkt i denna specifikation när dessa finns på nationell eller internationell nivå.* 

### event

Reguljärt uttryck: **`/^https?:\/\/[^\s\/$.?#].[^\s]*$/`**

Anger länk till kommande evenemang eller en ingångssida för evenemangsschema.

### capacity

Reguljärt uttryck: **`/^-?\d+$/`**

Anger max antalet individer som kan vara i evenemangslokalen. 

### opens

Reguljärt uttryck: **`/^(?:[01]\d|2[0-3]):[0-5]\d$/`**

Anger när lokalen öppnar enligt [time](#time).

### closes 

Reguljärt uttryck: **`/^(?:[01]\d|2[0-3]):[0-5]\d$/`**

Anger när lokalen öppnar enligt [time](#time).

### openinghours 

Reguljärt uttryck: **`/.+/`**

Ange ytterligare beskrivning av evenemangslokalens öppettider som inte beskrivs enligt attributen i tabellen.

### street

Reguljärt uttryck: **`/.+/`**

Anger evenemangslokalens adress.

### postalcode

Reguljärt uttryck: **`/^-?\d+$/`**

Anger postnummer. Ange inte mellanslag. 

### city

Reguljärt uttryck: **`/.+/`**

Anger kommunen där evenemangslokalen finns.

### email

Reguljärt uttryck: **`/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/`**

Anger funktionsadress till den organisation som ansvarar för information om evenemangslokalen. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: example@domain.se

### url

Reguljärt uttryck: **`/^https?:\/\/[^\s\/$.?#].[^\s]*$/`**

Anger ingångssidan till mer information om evenemangslokalen, ange inledande schemat https://

### image 

Anger en eller flera länkar till bilder på evenemangslokalen, ange inledande schemat https:// och mellanslag om ni vill dela flera bilder än en. 