# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
  {
    "id": 2454,
    "source": 9876543210,
    "type": "PerformingArtsTheater",
    "latitude": 55.604981,
    "longitude": 13.003822,
    "name": "Malmö City Theatre",
    "description": "En historisk teater med moderna produktioner",
    "amenities": "Wi-Fi",
    "accessibilityfeature": "Handikapptoalett",
    "event": "https://example.com/event/456",
    "capacity": 500,
    "opens": "17:00",
    "closes": "22:00",
    "openinghours": "Öppet mån-lör 17:00-22:00, söndagar stängt",
    "street": "Stortorget 1",
    "postalcode": "211 22",
    "city": "Malmö",
    "email": "info@malmotheatre.com",
    "URL": "https://www.malmotheatre.com/",
    "image": "https://example.com/images/malmotheatre.jpg"
  },
```
